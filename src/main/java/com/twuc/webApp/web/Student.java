package com.twuc.webApp.web;

import javax.validation.constraints.NotNull;

public class Student {
    @NotNull
    private String name;
    private Integer age;
    private String klass;

    public Student(String name, Integer age, String klass) {
        this.name = name;
        this.age = age;
        this.klass = klass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getKlass() {
        return klass;
    }

    public void setKlass(String klass) {
        this.klass = klass;
    }
}
