package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class BindingController {

    @GetMapping("/users/{userId}/books")
    public String get(@PathVariable Integer userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/users/int/{userId}/books")
    public String get(@PathVariable int userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String get(@PathVariable long userId, @PathVariable long bookId) {
        return "The book for user " + userId + " is No." + bookId;
    }

    @GetMapping("/users/query")
    public String get_request(@RequestParam String request) {
        return "It is an " + request;
    }

    @GetMapping("/users/collection")
    public String get_request(@RequestParam ArrayList<String> request) {
        return "The list as follows: " + request.toString();
    }

    @PostMapping("/datetimes")
    public Date get_date_time(@RequestBody Date datetime){
        return datetime;
    }

    @PostMapping("/users/student")
    public String get_student_request(@RequestBody @Valid Student student){
        return student.getName();
    }
}
