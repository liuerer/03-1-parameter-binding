package com.twuc.webApp.web;

class Contract {

    private String content;

    public Contract() {}

    public Contract(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
