package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BindingControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_request_when_binding_param_to_Integer_type() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_request_when_binding_param_to_int_type() throws Exception {
        mockMvc.perform(get("/api/users/int/2/books"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_request_when_binding_two_params() throws Exception {
        mockMvc.perform(get("/api/users/2/books/10"))
                .andExpect(status().isOk())
                .andExpect(content().string("The book for user 2 is No.10"));
    }

    @Test
    void should_get_request_by_query_string() throws Exception{
        mockMvc.perform(get("/api/users/query?request=apple"))
                .andExpect(status().isOk())
                .andExpect(content().string("It is an apple"));
    }

    @Test
    void should_get_request_by_query_without_given_param() throws Exception{
        mockMvc.perform(get("/api/users/query"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_request_by_query_with_collection_param() throws Exception{
        mockMvc.perform(get("/api/users/collection?request=1,2,3"))
                .andExpect(status().isOk())
                .andExpect(content().string("The list as follows: [1, 2, 3]"));
    }

    @Test
    void Serialize_contract_to_string() throws JsonProcessingException {
        Contract value = new Contract("test value");
        String serializeString = new ObjectMapper().writeValueAsString(value);
        assertEquals("{\"content\":\"test value\"}", serializeString);
    }

    @Test
    void unSerialize_string_to_contract() throws IOException {
        String serializedString = "{\"content\":\"test value\"}";
        Contract value = new ObjectMapper().readValue(serializedString, Contract.class);
        assertEquals("test value", value.getContent());
    }

//    @Test
//    void post_date_time() throws Exception {
//        mockMvc.perform(post("/api/datetimes")
//                .content("{dateTime: 2019-10-01T10:00:00Z}")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(content().json("{datetime: 2019-10-01T10:00:00Z}"));
//    }

    @Test
    void should_get_request_with_not_Null() throws Exception{
        mockMvc.perform(post("/api/users/student")
                .content("{\"name\":\"xiaoli\",\"age\":16,\"klass\":\"1\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }
}
